using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Linq;
using WeatherApi.Controllers;
using Xunit;

namespace WeatherApi.Tests
{
    public class WeatherForecastControllerTests
    {
        [Fact]
        public void Get_ReturnsWeatherForecasts()
        {
            // Arrange
            var loggerMock = new Mock<ILogger<WeatherForecastController>>();
            var controller = new WeatherForecastController(loggerMock.Object);

            // Act
            var result = controller.Get();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(5, result.Count());

            foreach (var forecast in result)
            {
                Assert.InRange(forecast.TemperatureC, -20, 55);
                Assert.Contains(forecast.Summary, WeatherForecastController.Summaries);
            }
        }

        [Fact]
        public void Get_ReturnsDifferentDatesForForecasts()
        {
            // Arrange
            var loggerMock = new Mock<ILogger<WeatherForecastController>>();
            var controller = new WeatherForecastController(loggerMock.Object);

            // Act
            var result = controller.Get();

            // Assert
            var dates = result.Select(forecast => forecast.Date).Distinct();
            Assert.Equal(5, dates.Count());
        }

        [Fact]
        public void Get_ReturnsCorrectDateRangeForForecasts()
        {
            // Arrange
            var loggerMock = new Mock<ILogger<WeatherForecastController>>();
            var controller = new WeatherForecastController(loggerMock.Object);

            // Act
            var result = controller.Get().ToList();

            // Assert
            for (int i = 0; i < result.Count; i++)
            {
                var expectedDate = DateTime.Now.AddDays(i + 1).Date;
                Assert.Equal(expectedDate.Day, result[i].Date.Day);
            }
        }
    }
}
